import React from 'react'
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

export const Alert = ({ openAlert, HandleClose }) => {
  return (
    <Dialog
        open={openAlert}
        onClose={HandleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Alert"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Nama/Alamat/Hobi tidak boleh kosong!
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={HandleClose}>OK</Button>
        </DialogActions>
      </Dialog>
  )
}
